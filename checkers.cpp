#include "checkers.hpp"


const CheckersBoard GameState::oneDimensialInitBoard = {
    1, 0, 1, 0, 1, 0, 1, 0,
    0, 1, 0, 1, 0, 1, 0, 1,
    1, 0, 1, 0, 1, 0, 1, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 2, 0, 2, 0, 2, 0, 2,
    2, 0, 2, 0, 2, 0, 2, 0,
    0, 2, 0, 2, 0, 2, 0, 2
};

void GameState::init(const int whichTurn, const CheckersBoard & fields) {
    turn = whichTurn;
    for (int x = 0; x < 8; x++) {
        board.push_back(std::vector<int>(8, 0));
        for (int y = 0; y < 8; y++) {
            board[x][y] = fields[8*y + x];
        }
    }
}

bool GameState::isNotEnd() const {
    if (getMoves().size() > 0) return true;
    return false;
}


CheckersBoard GameState::getBoard() const {
    CheckersBoard board_1D;
    for (size_t y = 0; y < 8; y++) {
        for (size_t x = 0; x < 8; x++) {
            board_1D[8*y+x] = board[x][y];
        }
    }
    return board_1D;
}

bool GameState::isPlayerMove(int player) const {
    return turn == player;
}

std::vector<GameMove> GameState::getMoves() const {
    bool betweenBeats = false;
    if (lastMoves.size() > 0 && !lastMoves.back().changedPlayer) {
        betweenBeats = true;
    }
    std::vector<GameMove> moves;
    for (size_t x = 0; x < 8; x++) {
        for (size_t y = 0; y < 8; y++) {
            if (!isPlayerColor(board[x][y])) {
                continue;
            }
            if (betweenBeats && ( x != lastMoves.back().x2 || y != lastMoves.back().y2) ) {
                continue;
            }
            std::vector<GameMove> movesFromField = getMovesFromField(x, y);
            if (movesFromField.size() > 0) {
                std::move(movesFromField.begin(), movesFromField.end(), std::back_inserter(moves));
            }
        }
    }
    std::vector<GameMove> beaten = moves;
    beaten.erase(std::remove_if(beaten.begin(), beaten.end(), [](const GameMove& m){return m.xBeaten == -1;}), beaten.end());
    if (beaten.size() > 0) return beaten;
    return moves;
}

std::experimental::optional<int> GameState::evaluate(int playerId) const {
    int player1 = 0;
    int player2 = 0;
    for (const auto& row : board) {
        for (const auto& field : row) {
            if (isColorOfPlayer(field, 1)) {
                player1 += 1;
                if (field > 10) {
                    player1 += 2;
                    if (playerId == 2) {
                        player1 += 2;
                    }
                }
            }
            if (isColorOfPlayer(field, 2)) {
                player2 += 1;
                if (field > 10) {
                    player2 += 2;
                    if (playerId == 1) {
                        player2 += 2;
                    }
                }
            }
        }
    }
    if (player1 == 0) {
        player2 = 1000;
    }
    if (player2 == 0) {
        player1 = 1000;
    }
    int delta = player2 - player1;
    if (playerId == 1) {
        delta = -delta;
    }
    return delta;
}

    
void GameState::reverseMove(const GameMove& move) {
    if (move.changedPlayer) {
        turn = turn==1?2:1;
    }
    if (move.xBeaten > 0) {
        board[move.xBeaten][move.yBeaten] = move.colorBeaten;
    }
    board[move.x1][move.y1] = move.color;
    board[move.x2][move.y2] = 0;
    lastMoves.pop_back();
}

void GameState::makeMove(GameMove& move) {
    move.color = board[move.x1][move.y1];
    board[move.x1][move.y1] = 0;
    board[move.x2][move.y2] = move.color;
    bool continueMove = false;
    if (move.xBeaten >= 0) {
        move.colorBeaten = board[move.xBeaten][move.yBeaten];
        board[move.xBeaten][move.yBeaten] = 0;
        std::vector<GameMove> moves = getMovesFromField(move.x2, move.y2);
        for (auto& m : moves) {
            if (m.xBeaten >= 0) {
                continueMove = true;
                break;
            }
        }
    }
    if (!continueMove) {
        if (turn == 1 && move.y2 == 7) {
            board[move.x2][move.y2] = 11;
        }
        if (turn == 2 && move.y2 == 0) {
            board[move.x2][move.y2] = 12;
        }
        turn = turn==1?2:1;
    }
    move.changedPlayer = !continueMove;
    lastMoves.push_back(move);
}

void GameState::queenCheck(int x, int y, int dx, int dy, std::vector<GameMove>& moves) const {
    int k = 1;
    while (check(x+dx*k, y+dy*k) == 0) {
        moves.push_back(GameMove(x, y, x+dx*k, y+dy*k, -1, -1));
        k += 1;
    }
    if (isOpponentColor(check(x+dx*k, y+dy*k)) && check(x+dx*(k+1), y+dy*(k+1))==0) {
        moves.push_back(GameMove(x, y, x+dx*(k+1), y+dy*(k+1), x+dx*k, y+dy*k));                
    }
}

std::vector<GameMove> GameState::getMovesFromField(int x, int y) const {
    std::vector<GameMove> moves;
    int d = turn==1?1:-1;
    if (check(x, y) > 10) {
        queenCheck(x, y, 1, 1, moves);
        queenCheck(x, y, -1, 1, moves);
        queenCheck(x, y, 1, -1, moves);
        queenCheck(x, y, -1, -1, moves);
    } else {
        if (check(x-1, y+d*1) == 0) {
            moves.push_back(GameMove(x, y, x-1, y+d*1, -1, -1));
        }
        if (check(x+1, y+d*1) == 0) {
            moves.push_back(GameMove(x, y, x+1, y+d*1, -1, -1));
        }

        if (isOpponentColor(check(x+1, y+d*1)) && check(x+2, y+d*2) == 0) {
            moves.push_back(GameMove(x, y, x+2, y+d*2, x+1, y+d*1));
        }
        if (isOpponentColor(check(x-1, y+d*1)) && check(x-2, y+d*2) == 0) {
            moves.push_back(GameMove(x, y, x-2, y+d*2, x-1, y+d*1));
        }
        if (isOpponentColor(check(x+1, y-d*1)) && check(x+2, y-d*2) == 0) {
            moves.push_back(GameMove(x, y, x+2, y-d*2, x+1, y-d*1));
        }
        if (isOpponentColor(check(x-1, y-d*1)) && check(x-2, y-d*2) == 0) {
            moves.push_back(GameMove(x, y, x-2, y-d*2, x-1, y-d*1));
        }
    }
    return moves;
}

bool GameState::isColorOfPlayer(const int color, const int playerId) const {
    int c = playerId==1 ? 1 : 2;
    return color%10 == c;
}

bool GameState::isPlayerColor(const int color) const {
    int c = turn==1 ? 1 : 2;
    return color%10 == c;
}

bool GameState::isOpponentColor(const int color) const {
    int c = turn==1 ? 2 : 1;
    return color%10 == c;
}

int GameState::check(const int x, const int y) const {
    if (x < 0 || x >= 8 || y < 0 || y >= 8) return -1;
    return board[x][y];
}

