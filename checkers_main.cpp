#include <experimental/optional>
#include <vector>
#include <iostream>
#include "minmax.hpp"
#include "checkers.hpp"


std::experimental::optional<GameMove> findMove(const std::vector<GameMove>& moves, int x1, int y1, int x2, int y2) {
    std::experimental::optional<GameMove> move;
    for (const auto& m : moves) {
        if (m.x1 == x1 && m.y1 == y1 && m.x2 == x2 && m.y2 == y2) {
            move = m;
            break;
        }
    }
    return move;
}

void printBoard(const GameState & game) {
    if (!game.isNotEnd()) {
        std::cout << 0 << " ";        
    } else {
        std::cout << (game.isPlayerMove(1) ? 1 : 2) << " ";
    }
    CheckersBoard board = game.getBoard();
    for (size_t y = 0; y < 8; y++) {
        for (size_t x = 0; x < 8; x++) {
            std::cout << board[y*8 + x];
            std::cout << " ";
        }
    }
    std::cout << "\n";
}

int main(int argc, char* argv[]) {
    GameState game;
    game.init();
    int turn = 0;
    int level[] = {0, 7, 7};
    if (argc > 2) {
        level[1] = std::atoi(argv[1]);
        level[2] = std::atoi(argv[2]);
    }
    printBoard(game);
    while (game.isNotEnd()) {
        turn = game.isPlayerMove(1) ? 1 : 2;            
        std::vector<GameMove> moves = game.getMoves();
        std::experimental::optional<int> min;
        std::experimental::optional<int> max;
        if (level[turn] > 0) {
            std::string continue_game;
            std::cin >> continue_game;
            std::pair<std::experimental::optional<int>, GameMove> best = minMax<GameState, GameMove>(game, level[turn], min, max, turn);
            GameMove m = best.second;
            game.makeMove(best.second);
            printBoard(game);
        } else {
            std::vector<GameMove> moves = game.getMoves();
            int x1, y1, x2, y2;
            std::cin >> x1 >> y1 >> x2 >> y2;
             std::experimental::optional<GameMove> m = findMove(game.getMoves(), x1, y1, x2, y2);
            if (m) {
                std::cout << "OK\n";
                game.makeMove(*m);
                printBoard(game);
            } else {
                std::cout << "NOT OK\n";
            }
        }
    }
    return 0;
}

/*
 *  Protokol gry: 
 *      >> 'OK'             // jesli tura AI
 *      << turn field_values 
 *      >> 'OK'             // jesli tura AI
 *      << turn field_values 
 *      >> x1 y1 x2 y2      // jesli tura gracza
 *      << 'OK' lub 'NOT OK'
 *      << turn field_values 
*/
