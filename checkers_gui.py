from threading import Thread

try:
    from PyQt5 import QtWidgets as gui, QtCore
    from PyQt5.QtGui import QPainter, QColor
except ImportError:
    from PyQt4 import QtGui as gui, QtCore
    from PyQt4.QtGui import QPainter, QColor


import sys
import psutil
import subprocess

s = 70
grid = [[] for i in xrange(8)]
player_id = 1
result = None
current_field = None
window = None
player_move = False

def parseInput():
    global grid, player_move
    line = map(int, result.stdout.readline().split())
    print(line)
    if line == []:
        return
    turn = line.pop(0)
    if turn != 0 and turn != player_id:
        print('print OK')
        result.stdin.write("OK\n")
    for i in xrange(8):
        grid[i] = line[i*8:i*8+8]
    if window is not None:
        window.update()
    if turn == 0:
        print('end')
        return
    if turn != player_id:
        player_move = False
        thread = Thread(target=parseInput)
        thread.start()
    else:
        player_move = True

def printBoard(board):
    light = QColor("#FFCC66")
    dark = QColor("#B33C00")
    white = QColor("#FFFFFF")
    black = QColor("#000000")
    white_queen = QColor("#969696")
    black_queen = QColor("#646464")
    current = QColor("#00FF00")

    for x in xrange(8):
        for y in xrange(8):
            color_of_field = grid[y][x]
            if player_id == 1:
                color_of_field = grid[7-y][7-x]
            color = light if (x+y) % 2 else dark
            board.setBrush(color)
            board.drawRect(x*s, y*s, s, s)
            if current_field is not None and current_field[0] == x and current_field[1] == y:
                board.setBrush(current)
                board.drawEllipse(QtCore.QPoint(x*s+s/2, y*s+s/2), s/2-1, s/2-1)
            if color_of_field == 11:
                board.setBrush(white_queen)
                board.drawEllipse(QtCore.QPoint(x*s+s/2, y*s+s/2), s/2-4, s/2-4)
                board.setBrush(white)
                board.drawEllipse(QtCore.QPoint(x*s+s/2, y*s+s/2), s/2-10, s/2-9)
            if color_of_field == 12:
                board.setBrush(black_queen)
                board.drawEllipse(QtCore.QPoint(x*s+s/2, y*s+s/2), s/2-4, s/2-4)
                board.setBrush(black)
                board.drawEllipse(QtCore.QPoint(x*s+s/2, y*s+s/2), s/2-10, s/2-9)

            if color_of_field == 1:
                board.setBrush(white)
                board.drawEllipse(QtCore.QPoint(x*s+s/2, y*s+s/2), s/2-4, s/2-4)
            if color_of_field == 2:
                board.setBrush(black)
                board.drawEllipse(QtCore.QPoint(x*s+s/2, y*s+s/2), s/2-4, s/2-4)

def check_if_valid():
    if result.stdout.readline() == 'OK\n':
        print('valid move')
        return True
    print('invalid move')
    return False

def makeMove(x1, y1, x2, y2):
    global current_field
    if player_id == 1:
        x1, y1, x2, y2 = 7-x1, 7-y1, 7-x2, 7-y2
    print x1, y1, x2, y2
    result.stdin.write("{} {} {} {}\n".format(x1, y1, x2, y2))
    current_field = None
    if check_if_valid():
        parseInput()
    else:
        window.update()

def check(x1, y1):
    global current_field
    x, y = x1, y1
    if player_id == 1:
        x, y = 7-x1, 7-y1
    if grid[y][x] % 10 != player_id:
        current_field = None
        return
    current_field = [x1, y1]
    window.update()

def mouse_clicked(x, y):
    global current_field
    if current_field is None:
        check(x/s, y/s)
    else:
        end_pos = [x/s, y/s]
        makeMove(current_field[0], current_field[1], end_pos[0], end_pos[1])
        current_field = None

def on_exit():
    proc = psutil.Process(result.pid)
    for p in proc.children():
        p.kill()
    proc.kill()

def menu_clicked():
    print 'menu item clicked'


class Window(gui.QMainWindow):

    def __init__(self):
        super(Window, self).__init__()
        self.initUI()


    def initUI(self):

        mainMenu = self.menuBar()
        mainMenu.setNativeMenuBar(True)
        menu = mainMenu.addMenu('Game')

        new_game_button = gui.QAction('New Game', self)
        new_game_button.triggered.connect(menu_clicked)
        menu.addAction(new_game_button)

        settingsButton = gui.QAction('Settings', self)
        settingsButton.triggered.connect(menu_clicked)
        menu.addAction(settingsButton)

        statisticsButton = gui.QAction('Statistics', self)
        statisticsButton.triggered.connect(menu_clicked)
        menu.addAction(statisticsButton)

        self.setWindowTitle('Checkers')
        self.setFixedSize(s*8, s*8)
        self.show()

    def paintEvent(self, e):
        global board
        qp = QPainter()
        qp.begin(self)
        printBoard(qp)
        qp.end()
        board = qp

    def mousePressEvent(self, e):
        if player_move:
            x, y = e.pos().x(), e.pos().y()
            mouse_clicked(x, y)

    def closeEvent(self, e):
        on_exit()

def main():
    global player_id, result, window

    level = 7
    if len(sys.argv) == 3:
        player_id = int(sys.argv[1])
        level = int(sys.argv[2])
    if player_id == 1:
        result = subprocess.Popen("./checkers 0 {}".format(level), shell=True, stdout=subprocess.PIPE, stdin=subprocess.PIPE)
    else:
        result = subprocess.Popen("./checkers {} 0".format(level), shell=True, stdout=subprocess.PIPE, stdin=subprocess.PIPE)

    parseInput()
    app = gui.QApplication(sys.argv)
    window = Window()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
