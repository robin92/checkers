cmake_minimum_required(VERSION 2.6)

set(CMAKE_CXX_FLAGS "-std=c++14 -O2")

# Locate GTest
find_package(GTest REQUIRED)
include_directories(${GTEST_INCLUDE_DIRS})

# Link runTests with what we want to test and the GTest and pthread library
add_executable(test tests.cpp checkers.cpp)
target_link_libraries(test ${GTEST_LIBRARIES} pthread)

add_executable(checkers checkers_main.cpp checkers.cpp)
